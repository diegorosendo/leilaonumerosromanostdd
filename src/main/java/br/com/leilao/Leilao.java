package br.com.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    List<Lance> lances;

    public Leilao() {
        lances = new ArrayList<Lance>();
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void adicionarNovoLance(Lance lance) throws RuntimeException {

        if(!validarLance(lance)) {
            throw new RuntimeException("Lance rejeitado");
        }

        this.lances.add(lance);
    }

    public boolean validarLance(Lance lance){
        if(this.lances.size() > 0) {
            if (lance.getValor() <= this.lances.get(this.lances.size() - 1).getValor()) {
                return false;
            }
        }
        return true;
    }

    public Lance obterMaiorLance() throws RuntimeException {
        if(this.lances.size() == 0) {
            throw new RuntimeException("Leilao não possui lance");
        }

        return this.lances.get(this.lances.size() - 1);

    }
}
