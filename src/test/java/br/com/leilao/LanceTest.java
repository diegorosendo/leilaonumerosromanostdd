package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LanceTest {

    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        leiloeiro = new Leiloeiro("Ze do leiláo", leilao);
    }

    @Test
    public void testaLanceInicial(){
        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 100.00);

        Assertions.assertTrue(leilao.validarLance(lance));

    }

    @Test
    public void testaLanceValido(){
        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 100.00);

        leilao.adicionarNovoLance(lance);

        Usuario usuarioSegundo = new Usuario(2, "Usuario 2");
        Lance lanceSegundo = new Lance(usuarioSegundo, 150.00);

        Assertions.assertTrue(leilao.validarLance(lanceSegundo));
    }

    @Test
    public void testaLanceInvalido(){
        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 100.00);

        leilao.adicionarNovoLance(lance);

        Usuario usuarioSegundo = new Usuario(2, "Usuario 2");
        Lance lanceSegundo = new Lance(usuarioSegundo, 50.00);

        Assertions.assertFalse(leilao.validarLance(lanceSegundo));

    }


}
