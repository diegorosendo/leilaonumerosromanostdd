package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeiloeiroTest {

    private Leiloeiro leiloeiro;
    private Leilao leilao;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
        leiloeiro = new Leiloeiro("Ze do leiláo", leilao);
    }

    @Test
    public void testaObterMaiorLanceLeilaoSemLance(){

        // Testa se tem lance para retornar
        Assertions.assertThrows(RuntimeException.class, () -> {Lance lanceRetornado = leiloeiro.retornarMaiorLance();});

    }


    @Test
    public void testaObterMaiorLanceLeilaoUmLance(){

        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 100.00);

        leilao.adicionarNovoLance(lance);

        // Testa se tem lance para retornar
        Assertions.assertEquals(100, leiloeiro.retornarMaiorLance().getValor());
    }


    @Test
    public void testaObterMaiorLanceLeilaoMaisDeUmLance(){

        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 100.00);
        leilao.adicionarNovoLance(lance);

        Usuario usuarioSegundo = new Usuario(2, "Usuario 2");
        Lance lanceSegundo = new Lance(usuarioSegundo, 200.00);
        leilao.adicionarNovoLance(lanceSegundo);

        // Testa se tem lance para retornar
        Assertions.assertEquals(200, leiloeiro.retornarMaiorLance().getValor());
    }


}
