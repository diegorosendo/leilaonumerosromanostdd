package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTest {

    private Leilao leilao;

    @BeforeEach
    public void setUp(){
        leilao = new Leilao();
    }

    @Test
    public void testaAdicionarLancesCrescentesAoLeilao(){

        Usuario usuarioJoao = new Usuario(1, "João");
        Usuario usuarioJose = new Usuario(1, "José");
        Usuario usuarioJPedro = new Usuario(1, "Pedro");

        Lance lanceInicialJoao = new Lance(usuarioJoao, 100.50);
        leilao.adicionarNovoLance(lanceInicialJoao);
// Testa se o  inicial lance foi aceito (Não gera Exception
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lanceInicialJoao);});


        Lance lanceSegundoJose = new Lance(usuarioJose, 200.50);
        leilao.adicionarNovoLance(lanceSegundoJose);
// Testa se o segundo lance foi aceito (Não gera Exception)
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lanceSegundoJose);});

        Lance lanceTerceiroPedro = new Lance(usuarioJPedro, 300.50);
        leilao.adicionarNovoLance(lanceTerceiroPedro);
// Testa se o terceiro lance foi aceito (Não gera Exception)
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lanceTerceiroPedro);});


// Testa se o valor do terceiro lance é de fato o maior
        Assertions.assertEquals(300.50, leilao.obterMaiorLance().getValor());


    }




    @Test
    public void testaRejeitarLanceMenorOuIgualNoLeilao(){

        Usuario usuarioJoao = new Usuario(1, "João");
        Usuario usuarioJose = new Usuario(1, "José");

        Lance lanceInicialJoao = new Lance(usuarioJoao, 200.50);
        leilao.adicionarNovoLance(lanceInicialJoao);
// Testa se o  inicial lance foi aceito (Não gera Exception
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lanceInicialJoao);});


        Lance lanceSegundoJose = new Lance(usuarioJose, 100.50);
        leilao.adicionarNovoLance(lanceSegundoJose);
// Testa se o segundo lance foi aceito (Não gera Exception)
        Assertions.assertThrows(RuntimeException.class, () -> {leilao.adicionarNovoLance(lanceSegundoJose);});

    }


}
